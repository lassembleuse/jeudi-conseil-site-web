# Programme et ressources pour les jeudi conseil de Kejal

En juin 2019, l'assembleuse (Claire Zuliani) anime quelques sessions des [jeudi conseil](https://www.kejal.fr/le-petit-pre-lieu-partage/au-quotidien/les-ateliers-conseils-du-jeudi/) autour de la conception de site web pour un public d'entrepreneur.e de l’économie solidaire et du développement durable.

Ce dossier regroupe le programme de ses sessions ainsi que des ressources en lien, ainsi que probablement quelques notes à la volée + ou - en vrac.

Bien entendu, le contenu est plus que susceptible d'évoluer d'ici juin, et même après :)

## Les sessions

- [Jeudi 13 juin - Je veux un site web !](19-06-13/je-veux-un-site-web.md)
- [Jeudi 20 juin - Je veux faire mon site web moi-même ! - session 1](19-06-20/je-veux-faire-mon-site-web.md)
- [Jeudi 4 juillet - Je veux faire mon site web moi-même ! - session 2](19-07-04/je-veux-faire-mon-site-web.md)