# Je veux un site web ! - Programme

*Programme en cours de construction*

## Principe

- privilégier vos questions, retours. C'est aussi vous qui allez influer le cours de l'atelier. Si le "programme" est pas tenu, c'est pas grave. Je reste là cet aprem pour ceux qui veulent, et je mets à dispo prises de note pour ceux qui ne peuvent rester.

- je ne connais pas tout et je suis là aussi pour apprendre, avec vous et de vous. Si vous-mêmes vous avez testé / entendu parler de solutions ou outils, n'hésitez pas à prendre la parole. On est sur de l'échange de savoir, pas sur un cours pompeux descendant.

## Points à aborder

**~9h30**

### Tour de table
   
- quelle est votre activité ?
- vous avez déjà un site ? Ou une idée de comment vous allez vous y prendre ?

### Questions libres 
  
3 min pour noter sur post-it toutes vos questions. Posez-les sur le mur en les lisant.
Je les regroupe par thématique, et j'insisterai les points que vous aurez soulevés durant la session.

**~ 10h**

### Comment ça marche un site web ? 

- c'est composé de quoi ? 
  - Serveur, BDD, NDD
  - html, css, javascript, front/back, php, python, ruby...

### Les différentes composantes de votre site web.
- interface admin ? comptes utilisateurs ? composantes dynamiques (SPA) ?

### Ce à quoi il faut penser

- rédactionnel prend du temps
- images
- animation et gestion de communauté

### Délais / coût / fiabilité -> inconciliable

### Prestataire en régie / au forfait / méthodes agiles


## Besoins
- paper board ou tableau blanc
- video proj
- code wifi à partager

## Ressources et inspirations pour moi

- Les questions des coopanamiens pour la journée numérique de Numéricoop du 19/03/19 : https://annuel.framapad.org/p/Numericoop19mars_propositionsdesujets

## Ressources pour les participants