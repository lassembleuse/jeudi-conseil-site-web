# Je veux faire mon site web moi-même ! - Programme

*Programme en cours de construction*

## Principe

- privilégier vos questions, retours. C'est aussi vous qui allez influer le cours de l'atelier. Si le "programme" est pas tenu, c'est pas grave. Je reste là cet aprem pour ceux qui veulent, et je mets à dispo prises de note pour ceux qui ceux qui ne peuvent rester.

- je ne connais pas tout et je suis là aussi pour apprendre, avec vous et de vous. Si vous-mêmes vous avez testé / entendu parler de solutions ou outils, n'hésitez pas à prendre la parole. On est sur de l'échange de savoir, pas sur un cours pompeux descendant.

## Points à aborder

TODO

## Besoins
- paper board ou tableau blanc
- video proj
- code wifi à partager

## Ressources et inspirations pour moi


## Ressources pour les participants