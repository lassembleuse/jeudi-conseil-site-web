# Jeudi 20 juin - Je veux faire mon site web moi-même ! - session 1

**Je veux faire mon site web moi-même !** 

Etre autonome en développant soi-même son site web.


## Présentation

Vous souhaitez être le plus autonome possible avec votre site web ? Vous voulez avoir un site web aussi responsable, éthique et respectueux de l'environnement que possible ? Les questions de rapidité et de performance sont importantes pour vous ? L'idée de faire vous même votre site internet vous titille mais vous ne savez pas par où commencer ?

A travers la création d'un site web statique, rapide et performant, offrez une vitrine à votre activité et comprenez par la pratique le fonctionnement d'un site web.

Durant cette première session, nous poserons les bases de votre site.


### 9h30 à 12h30 - atelier

Dans la matinée, nous vous mettons le pied à l'étrier pour commencer le développement de  votre site web statique.

Notions abordées :
- Pourquoi un site statique ?
  - simple et pédagogique (retour aux sources du web)
  - performant, sûr et écologique
  - prototyper et tester rapidement une idée
- Ce pour quoi un site statique n'est PAS pertinent
- Bases de html et css
- Architecture et hyperliens
- A la découverte de git et de la gestion de version (outil de développement moderne)
- Mise en production


### 13h30 à 16h30 - session libre

Dans l'après-midi, je reste à votre disposition pour vous aider à poursuivre le développement de votre site.